1) Write Servlet application to print current date & time?

Ref : use simple Date object

sol:

package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/AssignmentDateServlet")
public class AssignmentDateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 PrintWriter out=response.getWriter();
        
        java.util.Date date = new java.util.Date();
        out.println("<h1>"+"Current Date & Time: " +date.toString()+"</h1>");
	}



}


-----------------------------------------------------------------------------------------------
2) Write a servlet application to establish communication between html and servlet page using hyperlink.

Ref : <a href = "http://localhost:8080/ServletApp/anchortag">check</a>



sol:

package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/AssignmentDateServlet")
public class AssignmentDateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 PrintWriter out=response.getWriter();
        
        java.util.Date date = new java.util.Date();
        out.println("<h1>"+"Current Date & Time: " +date.toString()+"</h1>");
	}



}



<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Assignment Html</title>
</head>
<body>
<form>
<form action="AssignmentDateServlet"> 
<a href="http://localhost:8085/Concepts/AssignmentDateServlet">Click to See Date and Time </a>
</form> 
</form> 
</body>
</html>




-----------------------------------------------------------------------------------------------


3) Write a servlet that refreshes the browser every second and current time will be changed automatically.

Ref : GregorianCalendar , response header

sol:

package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Refresh")
public class Refresh extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		  Calendar calendar = new GregorianCalendar();
          String am_pm;
          int hour = calendar.get(Calendar.HOUR);
          int minute = calendar.get(Calendar.MINUTE);
          int second = calendar.get(Calendar.SECOND);
          //Calendar.AM_PM returns 0 if it is AM and 1 for PM 
          if(calendar.get(Calendar.AM_PM) == 0)
               am_pm = "AM";
          else
               am_pm = "PM";

          String CurrentTime = hour+":"+ minute +":"+ second +" "+ am_pm;
    
          PrintWriter out = response.getWriter();
      
          out.println("<h1 align='center'>Auto Refresh Page</h1><hr>");
          out.println("<h3 align='center'>Current time: "+CurrentTime+"</h3>");
		
		
	}



}



<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Assignment Html</title>
</head>
<body>

<form action="Refresh">
<a href="http://localhost:8085/Concepts/Refresh">Refreshing Date and Time </a>
</form> 
</form> 
</body>
</html>

-----------------------------------------------------------------------------------------------


4) Write a Servlet application to count the total number of visits on the website.


package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/CountServlet")
public class CountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	 private int count;

  
     public void init() throws ServletException
     {
          count = 0;
     }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		//response.setContentType("text/html");
		PrintWriter out =  response.getWriter();
        out.println("<h3>Welcome to my website !</h3><hr>");
        out.println("You are visitor number: "+ (++count));
        out.println("</form>");
	}

}


<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Assignment Html</title>
</head>
<body>
<form action="CountServlet">
<a href="http://localhost:8085/Concepts/CountServlet">CountServlet</a> 
</form> 
</body>
</html>

-----------------------------------------------------------------------------------------------


5) Write a Servlet program that accepts the age and name and displays if the user is eligible for voting or not.


package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/VerifyingAge")
public class VerifyingAge extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		PrintWriter out=response.getWriter();
		String name=request.getParameter("name");
		int age=Integer.parseInt(request.getParameter("age"));
	
		if(age>=18)
		{
			out.println(name+" is eligible to vote");			
		}else {
	         out.println(name+ " is not eligible to vote");
		}
	}
	}





<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Assignment Html</title>
</head>
<body>
<form action="VerifyingAge">
Name:<input type="text" name="name"/><Br/>
Age:<input type="text" name="age"/><Br/>
<input type="submit"/> 
</form> 
</body>
</html>

-----------------------------------------------------------------------------------------------


6) Write an application to demonstrate the session tracking in Servlet with following details
package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/SessionTracking")
public class SessionTracking extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
        
	      // Get session creation time.
	      Date createTime = new Date(session.getCreationTime());
	         
	      // Get last access time of this web page.
	      Date lastAccessTime = new Date(session.getLastAccessedTime());

	      String title = "Welcome Back to my website";
	      Integer visitCount = new Integer(0);
	      String visitCountKey = new String("visitCount");
	      String userIDKey = new String("userID");
	      String userID = new String("ABCD");

	      // Check if this is new comer on your web page.
	      if (session.isNew()) {
	         title = "Welcome to my website";
	         session.setAttribute(userIDKey, userID);
	      } else {
	         visitCount = (Integer)session.getAttribute(visitCountKey);
	         visitCount++;
	         userID = (String)session.getAttribute(userIDKey);
	      }
	      session.setAttribute(visitCountKey,  visitCount);

	      // Set response content type
	      response.setContentType("text/html");
	      PrintWriter out = response.getWriter();


	      out.println( "<!doctype html public \"-//w3c//dtd html 4.0 " +
	 	         "transitional//en\">\n" +
	         "<html>\n" +            
	            "<body bgcolor = \"#fffeee\">\n" + "<h1 align = \"center\">" + title + "</h1>\n" +
	               "<h2 align = \"center\">Session Infomation</h2>\n" +
	               "<table border = \"1\" align = \"center\">\n" +
	                  
	                  "<tr bgcolor = \"#fffeee\">\n" + "<th>Session info</th><th>value</th></tr>\n" +
	                     
	                  "<tr>\n" + "<td>id</td>\n" +" <td>" + session.getId() + "</td>  </tr>\n" +
	                  
	                  "<tr>\n" +"<td>Creation Time</td>\n" + "<td>" + createTime + "  </td> </tr>\n" +
	                  
	                  "<tr>\n" + "<td>Time of Last Access</td>\n" +"<td>" + lastAccessTime + "</td> </tr>\n" +
	                  
	                  "<tr>\n" +"<td>User ID</td>\n" +"<td>" + userID + "</td> </tr>\n" +
	                  
	                  "<tr>\n" +"<td>Number of visits</td>\n" + "<td>" + visitCount + "</td> </tr>\n" +
	               "</table>\n" +  "</body> </html>"
	      );
	}

}

-----------------------------------------------------------------------------------------------



6) Write an application to demonstrate the session tracking in Servlet with following details
package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/SessionTracking")
public class SessionTracking extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
        
	      // Get session creation time.
	      Date createTime = new Date(session.getCreationTime());
	         
	      // Get last access time of this web page.
	      Date lastAccessTime = new Date(session.getLastAccessedTime());

	      String title = "Welcome Back to my website";
	      Integer visitCount = new Integer(0);
	      String visitCountKey = new String("visitCount");
	      String userIDKey = new String("userID");
	      String userID = new String("ABCD");

	      // Check if this is new comer on your web page.
	      if (session.isNew()) {
	         title = "Welcome to my website";
	         session.setAttribute(userIDKey, userID);
	      } else {
	         visitCount = (Integer)session.getAttribute(visitCountKey);
	         visitCount++;
	         userID = (String)session.getAttribute(userIDKey);
	      }
	      session.setAttribute(visitCountKey,  visitCount);

	      // Set response content type
	      response.setContentType("text/html");
	      PrintWriter out = response.getWriter();


	      out.println( "<!doctype html public \"-//w3c//dtd html 4.0 " +
	 	         "transitional//en\">\n" +
	         "<html>\n" +            
	            "<body bgcolor = \"#fffeee\">\n" + "<h1 align = \"center\">" + title + "</h1>\n" +
	               "<h2 align = \"center\">Session Infomation</h2>\n" +
	               "<table border = \"1\" align = \"center\">\n" +
	                  
	                  "<tr bgcolor = \"#fffeee\">\n" + "<th>Session info</th><th>value</th></tr>\n" +
	                     
	                  "<tr>\n" + "<td>id</td>\n" +" <td>" + session.getId() + "</td>  </tr>\n" +
	                  
	                  "<tr>\n" +"<td>Creation Time</td>\n" + "<td>" + createTime + "  </td> </tr>\n" +
	                  
	                  "<tr>\n" + "<td>Time of Last Access</td>\n" +"<td>" + lastAccessTime + "</td> </tr>\n" +
	                  
	                  "<tr>\n" +"<td>User ID</td>\n" +"<td>" + userID + "</td> </tr>\n" +
	                  
	                  "<tr>\n" +"<td>Number of visits</td>\n" + "<td>" + visitCount + "</td> </tr>\n" +
	               "</table>\n" +  "</body> </html>"
	      );
	}

}

-----------------------------------------------------------------------------------------------

7) Write a Servlet application for login page, which is check the username and password. If username and password are matched, display welcome message along with username

Ref : login.html and LoginServlet . username and password can be hard coded or retrieved from database through service and dao layer?



sol:


package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/PasswordChecking")
public class PasswordChecking extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out=response.getWriter();
		String name=request.getParameter("name");
		String password =request.getParameter("password");
	
		if(name.equals("admin") && password.equals("admin"))
		{
			out.println("valid");
			
			
		}else {
	         out.println("invalid");
		}
	}


}



<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
</head>
<body>
<form action="PasswordChecking">
UserName:<input type="text" name="name"/><Br/>
Password:<input type="password" name="password"/><Br/>
<input type="submit" value="submit"/>
</form>
</body>
</html>



-----------------------------------------------------------------------------------------------

8) Develop a registration page in servlet with proper validation and store all records in the database.

Ref

· create table registration (registerid int primary key , first_name varchar(20), email varchar(20), mobile number, dob DATE ,gender varchar(6), country varchar (10));

· INSERT INTO registration (first_name,email, mobile ,dob,gender,country) VALUES('John','john@hcl.com',’9988776655, '2019-03-01',’male’,’India’);

· a) register.html b) RegisterServlet.java c) web.xml d) RegisterService (interface) e)RegisterServiceImpl f) RegisterDao (interface) g)RegisterDaoImpl h)display.jsp





sol:



package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*create table registration(name varchar(20),password varchar(20),
email varchar(30),mobilenumber int,dob DATE,gender varchar(6),country varchar(20));*/

@WebServlet("/StudentRegistrationPage")
public class StudentRegistrationPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {  
         response.setContentType("text/html");  
         PrintWriter out = response.getWriter();  
         
         String name = request.getParameter("userName");  
         String pwd = request.getParameter("userPass");  
         String email = request.getParameter("userEmail");
         int mobile = Integer.parseInt(request.getParameter("userMobile"));
         String dob = request.getParameter("userDOB");  
         String gender = request.getParameter("gender");  
         String country =request.getParameter("userCountry");  
         
         try
         {  
              //load the driver
        	 Class.forName("com.mysql.cj.jdbc.Driver");
 			 Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/servlet","root","Vcetvcet@35");	 
              // create the prepared statement object
              PreparedStatement ps=con.prepareStatement("insert into registration values(?,?,?,?,?,?,?)");  
 
              ps.setString(1,name);  
              ps.setString(2,pwd);  
              ps.setString(3,email);  
              ps.setInt(4, mobile);
              ps.setString(5,dob);  
              ps.setString(6,gender);  
              ps.setString(7,country);  
 
              int i = ps.executeUpdate();  
              if(i>0)  
              out.print("You are successfully registered...");  
 
         }
         catch (Exception ex)
         {
              ex.printStackTrace();
         }  
         out.close();  
    }  

}



<!doctype html>  
   <body>  
      <form action="StudentRegistrationPage" method="post">  
         <fieldset style="width:50%; background-color:#ccffeb">
            <h2 align="center">Registration form</h2><hr>
            <table>
               <tr>
                  <td>Name</td>
                  <td><input type="text" name="userName" required /></td>
               </tr>  
               <tr>
                  <td>Password</td>
                  <td><input type="password" name="userPass" required /></td>
               </tr>  
               <tr>
                  <td>Email Id</td>
                  <td><input type="text" name="userEmail" required /></td>
               </tr>  
               <tr>
                  <td>Mobile</td>
                  <td><input type="text" name="userMobile" required/></td>
               </tr>  
               <tr>
                  <td>Date of Birth</td>
                  <td><input type="date" name="userDOB" required/></td>
               </tr>  
               <tr>
                  <td>Gender</td>
                  <td><input type="radio" name="gender" value="male" checked> Male
                  <input type="radio" name="gender" value="female"> Female </td></tr>
               <tr>
                  <td>Country</td>
                  <td><select name="userCountry" style="width:130px">  
                     <option>Select a country</option>  
                     <option>India</option>  
                     <option>America</option>  
                     <option>England</option>  
                     <option>other</option></select>
                  </td>
               </tr>
               <tr>
                  <td><input type="reset" value="Reset"/></td>
                  <td><input type="submit" value="Register"/></td>
               </tr>
            </table>
         </fieldset>  
      </form>  
   </body>  
</html>


-----------------------------------------------------------------------------------------------


10) Types of Session Tracking in Servlet

There are four main ways to manage the session in java web-application:

a. URL rewriting


package com.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/UrlRewritingDemo")
public class UrlRewritingDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
//sending information throught url
//you can disable cookies so that we use urlrewrite ...in the url itself we carry the information to the next page
	// keyword is  response.sendRedirect and symbols is  

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name=request.getParameter("username");
		String pass=request.getParameter("password");
		
		if(pass.equals("1234"))
		{
			response.sendRedirect("UrlRewritingDemo2?username="+name);
		}
	}


}



package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/UrlRewritingDemo2")
public class UrlRewritingDemo2 extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
        String user = request.getParameter("username");
        out.println("Welcome "+user);
		
	}

}



<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="UrlRewritingDemo" method="post">
UserName:<input type="text" name="username"/><Br/>
Password:<input type="password" name="password"/><Br/>
<input type="submit"/>
</form>
</body>
</html>


b. Cookies



package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CookieServlet")
public class CookieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		String name=request.getParameter("username");
		out.println("Hai"+name);
		//String encodecookie=URLEncoder.encode(name,"UTP-8");
		//if we want create cookie we create cookie class
		
		//Cookie cookie=new Cookie("name",encodecookie);
     	Cookie cookie=new Cookie("name",name);
		response.addCookie(cookie);
	
		out.println("<form action='CookieServlet2'>");
		out.println("<input type='submit' value='submit'>");
	
	}

}


package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CookieServlet2")
public class CookieServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//its stores the information in an array
		PrintWriter out=response.getWriter();
		Cookie cookiearray[]=request.getCookies();
		
		for(Cookie cookie:cookiearray)
		{
			out.println(cookie.getValue());			
		}
		
		//out.println(cookiearray[0].getValue());
	}

}


<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="CookieServlet">
UserName:<input type="text" name="username"/><Br/>

<input type="submit"/>
</form>
</body>
</html>





c. Hidden form fields

package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/HiddenFormFieldDemo")
public class HiddenFormFieldDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//sending information throught hidden textbox
	
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        String username=request.getParameter("username");
	        PrintWriter out=response.getWriter();
	        out.println(username);
	        response.setContentType("text/html");
	         
	         out.println("<form action='HiddenFormFieldDemo2'>");
	         //if type input type='submit' it will show on the page ,,its also a textbox only but its not visibel to user
	         out.println("<input type='hidden' name='username' value='"+username+"'>");
	         out.println("<input type='submit' value='submit'>");
	         out.println("</form>");
	       
	    }
       
    }



package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/HiddenFormFieldDemo2")
public class HiddenFormFieldDemo2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		PrintWriter out=response.getWriter();
        String user = request.getParameter("username");
        out.println("Welcome "+user);
	}

}




<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="HiddenFormFieldDemo" method="post">
UserName:<input type="text" name="username"/><Br/>
Password:<input type="password" name="password"/><Br/>
<input type="submit"/>
</form>
</body>
</html>

d. HTTPS

Implement the above individually in servlet


-----------------------------------------------------------------------------------------------

11) Create 3 links: login, logout and Register in a web application and from any page if the logout has clicked disable all the session related object and by default hold the session object for max of 15 min in an application.




package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/SessionManagementDemo")
public class SessionManagementDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
		PrintWriter out=response.getWriter();
	
		String user=request.getParameter("username");
		HttpSession session=request.getSession();
		
		// methods in httpsession ...setAttribute and getAttribute
		session.setAttribute("username", user);

		out.println("Home Page"+"<Br/>");
		out.println("<a href ='SessionManagementDemo2'>go to session 2</a> <Br/>");
		
	}

}



package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/SessionManagementDemo2")
public class SessionManagementDemo2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();	
		HttpSession session=request.getSession();
		
		//session.invalidate();
		out.println("hello "+session.getAttribute("username"));		
		out.println("<a href ='SessionManagementDemo3'>go to session 3</a> <Br/>");
		
		session.setMaxInactiveInterval(900);
	}



}



package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/SessionManagementDemo3")
public class SessionManagementDemo3 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();
		HttpSession session=request.getSession();
		session.invalidate();
		out.println("bye "+session.getAttribute("username"));
	
	}

}




   <!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Hi There</title>
</head>
<body>
<form action="SessionManagementDemo" method="post">
UserName:<input type="text" name="username"/><br/>
Password:<input type="password" name="password"/><br/>
<input type="submit" value="submit"/>
</form>
</body>
</html>


-----------------------------------------------------------------------------------------------

12) How to Retrieve Data from ServletConfig Interface Object.

Click on the button “Click Here”.

It should fetch the details of N1 and N2 from Web.xml and it display the output given below.



package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class RetrieveDataConfig extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");

		ServletConfig conf=getServletConfig();

		int n1=Integer.parseInt(conf.getInitParameter("n1"));
		int n2=Integer.parseInt(conf.getInitParameter("n2"));
		int s1=n1+n2;

		out.println("n1 value is " +n1+ " and n2 is " +n2 + "=" +s1);

	
	}

}

<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:web="http://xmlns.jcp.org/xml/ns/javaee" xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd">
  <servlet>
    <servlet-name>RetrieveDataConfig</servlet-name>
    <servlet-class>com.servlet.RetrieveDataConfig</servlet-class>
    <init-param>
      <param-name>n1</param-name>
      <param-value> 100 </param-value>
    </init-param>
    <init-param>
      <param-name>n2</param-name>
      <param-value> 200 </param-value>
    </init-param>
  </servlet>
  <servlet-mapping>
    <servlet-name>RetrieveDataConfig</servlet-name>
    <url-pattern>/RetrieveDataConfig</url-pattern>
  </servlet-mapping>
  <servlet>
</web-app>


-----------------------------------------------------------------------------------------------

13) How to Retrieve Data from ServletContext Interface Object.

Create a servlet application to connect with database .To establish the connection get URL,Username,password from web.xml and it should be common to all the servlet.

package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ServletContextDemo")
public class ServletContextDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		 PrintWriter out=response.getWriter();
		 String some= request.getServletContext().getInitParameter("username");
		 out.print(some);
		
		
		
	}

}


<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:web="http://xmlns.jcp.org/xml/ns/javaee" xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd">
 <context-param>
    <param-name>username</param-name>
    <param-value> black</param-value>
  </context-param>
  <context-param>
    <param-name>password</param-name>
    <param-value>1234</param-value>
  </context-param>
  <servlet>
</web-app>



<<<<<<< HEAD
=======

>>>>>>> f2f97dc21f8ba3cdd448b544ca729e29a1f9dab4
-----------------------------------------------------------------------------------------------


14)Create filter application for authentication



<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
</head>
<body>
<form action="FilterAssignment" method="post">
UserName:<input type="text" name="username"/><Br/>
Password:<input type="password" name="password"/><Br/>
<input type="submit"/>
</form>
</body>
</html>



package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;


@WebFilter("/FilterAssignment")
public class FilterAssignment2 implements Filter {


    public FilterAssignment2() {
        // TODO Auto-generated constructor stub
    }

	public void destroy() {
		// TODO Auto-generated method stub
	}


	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest res=(HttpServletRequest)request;	
		PrintWriter out=response.getWriter();
		String user=request.getParameter("username");
		String pass=request.getParameter("password");
		if(user.equals("admin") && pass.equals("admin"))
		{
			chain.doFilter(request, response);
		}
		else
		{
			out.println("Something wrong");
		}
	}

	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}




package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@WebServlet("/FilterAssignment")
public class FilterAssignment extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	     	PrintWriter out=response.getWriter();
		
			out.println("Your are logged in successfully");
		
	}




}
