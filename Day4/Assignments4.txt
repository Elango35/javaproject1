 DAY 4



Day 4 Java lab exercise

1.Create an abstract class named Shape with the following protected attributes / member variables.

String name

Include a 1-argument constructor.

Include getters and setters.

Include an abstract method named calculateArea() . This method returns a Float value.

Create a class named Circle . The class Circle is a derived class of Shape. Include the following private attributes / member variables.

Integer radius

Include a 2-argument constructor. The order of the arguments is name, radius.

Include getters and setters.

Override the abstract method calculateArea() defined in the Shape class. This method returns the area of the circle. [Take the value of pi as 3.14]

Create a class named Square . The class Square is a derived class of Shape. Include the following private attributes / member variables.

Integer side

Include a 2-argument constructor. The order of the arguments is name, side.

Include getters and setters.

Override the abstract method calculateArea() defined in the Shape class. This method returns the area of the square.

Create a class named Rectangle . The class Rectangle is a derived class of Shape. Include the following private attributes / member variables.

Integer length

Integer breadth

Include a 3-argument constructor. The order of the arguments is name, length, breadth

Include getters and setters.

Override the abstract method calculateArea() defined in the Shape class. This method returns the area of the rectangle.

Create another class called Main. In the method, create instances of the above classes and test the above classes.

All Float values are displayed correct to 2 decimal places.

All text in bold corresponds to input and the rest corresponds to output.

Sample Input and Output 1:

Circle

Square

Rectangle

Enter the shape name

Circle

Enter the radius

25

Area of Circle is 1962.50



sol:


package AbstractAndInterfaces;

import java.util.Scanner;

abstract class Shape
{
	protected String name;	
	
	Shape()
	{
		
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	Shape(String name2)
	{
		this.name=name;
	}	
	
	abstract float calculateArea();

}

class Circle extends Shape
{
	protected int radius;

	public int getRadius() 
	{
		return radius;
	}
	
	public void setRadius(int radius)
	{
		this.radius = radius;
	}
	
	public Circle(String name,int radius)
	{
		
		super(name);
		this.radius=radius;
	}
	
	@Override
	float calculateArea()
	{		
		return (float) (3.14*(radius*radius));		
	}	
}

class Square extends Shape
{
	protected int side;	
	
	Square(String name,int side)
	{
		super(name);
		this.side=side;
	}
	
	public int getSide() 
	{
		return side;
	}

	public void setSide(int side) 
	{
		this.side = side;
	}

	@Override
	float calculateArea()
	{
		return (float) (side*side);
		
	}
}

class Rectangle extends Shape
{
	protected int length;
	protected int breadth;
	
	Rectangle(String name,int length,int breadth)
	{
		super(name);
		this.length=length;
		this.breadth=breadth;
	}
	
	public int getLength() 
	{
		return length;
	}

	public void setLength(int length) 
	{
		this.length = length;
	}

	public int getBreadth() 
	{
		return breadth;
	}

	public void setBreadth(int breadth)
	{
		this.breadth = breadth;
	}

	@Override
	float calculateArea()
	{
		return (float) (length*breadth);	
	}
}

public class Main{

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Circle");
		System.out.println("Square");
		System.out.println("Rectangle");
		System.out.println("--------------------------------------------------------");
		
		
		System.out.println("Enter shape name");
		String strr=sc.nextLine();
		if(strr.equals("circle"))
		{
			System.out.println("Enter the radius");
			int p=sc.nextInt();
			Circle c=new Circle(strr, p);
			System.out.println(c.calculateArea());
			System.out.println("--------------------------------------------------------");
		}
		if(strr.equals("square"))
		{
			System.out.println("Enter the side");
			int e=sc.nextInt();
			Square s=new Square(strr,e);
			System.out.println(s.calculateArea());
			System.out.println("--------------------------------------------------------");
		}
		if(strr.equals("rectangle"))
		{
			System.out.println("Enter the length and breadth");
			int f=sc.nextInt();
			int g=sc.nextInt();
			Rectangle r=new Rectangle(strr,f,g);
			System.out.println(r.calculateArea());
		}
	}
}


o/p

Circle
Square
Rectangle
--------------------------------------------------------
Enter shape name
circle
Enter the radius
25
1962.5
--------------------------------------------------------


2. A Java interface can only contain method signatures and fields. The interface can be used to achieve polymorphism. In this problem, you will practice your knowledge on interfaces.

You are given an interface AdvancedArithmetic which contains a method signature int divisor_sum(int n). You need to write a class called MyCalculator which implements the interface.

divisorSum function just takes an integer as input and return the sum of all its divisors. For example divisors of 6 are 1, 2, 3 and 6, so divisor_sum should return 12. The value of n will be at most 1000.

Read the partially completed code in the editor and complete it. You just need to write the MyCalculator class only. Your class shouldn't be public.


sol:


package AbstractAndInterfaces;

public interface AdvancedArithmetic {

	int divisor_sum(int n);

}
package AbstractAndInterfaces;

class MyCalculator implements  AdvancedArithmetic {

	@Override
	public int divisor_sum(int n) 
	{
		int sum=1;
		for(int i=2;i<=n;i++)
		{
			if(n%i==0)
			{
				if(i!= n)
	                sum = sum + i + n % i;
	            else
	                sum = sum + i;
			}
		}
		return sum;
	}
	
	public static void main(String[] args) {
		
		MyCalculator myObj=new MyCalculator();
		System.out.println(myObj.divisor_sum(1000));
	}
}


o/p

2340


3.Create an abstract class named Card with the following protected attributes / member variables.

String holderName;

String cardNumber;

String expiryDate;

Include appropriate getters and setters.

Include appropriate constructors. In the 3-argument constructor, the order of the arguments is holderName, cardNumber, expiryDate.

Create a class named MembershipCard. The class MembershipCard is a derived class of Card. Include the following private attributes / member variables.

Integer rating

Include appropriate getters and setters.

Include appropriate constructors. In the 4-argument constructor, the order of the arguments is holderName, cardNumber, expiryDate, rating.

Create a class named PaybackCard. The class PaybackCard is a derived class of Card. Include the following private attributes / member variables.

Integer pointsEarned;

Double totalAmount;

Include appropriate getters and setters.

Include appropriate constructors. In the 5-argument constructor, the order of the arguments is holderName, cardNumber, expiryDate, pointsEarned, totalAmount.

Create another class called Main. In the method, create instances of the above classes and test the above classes. The card details are entered separated by a ‘|’.

All text in bold corresponds to input and the rest corresponds to output.

Sample Input and Output 1:

Select the Card

1.Payback Card

2.Membership Card

1

Enter the Card Details:

Anandhi|12345|14/01/2020

Enter points in card

1000

Enter Amount

50000

Anandhi's Payback Card Details:

Card Number 12345

Points Earned 1000

Total Amount 50000.0


sol:


package AbstractAndInterfaces;

import java.util.Scanner;

abstract class Card{
	
	protected String holderName;
	protected String cardNumber;
	protected String expiryDate;
	
	Card()
	{
		
	}
	
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	Card(String holderName,String cardNumber,String expiryDate)
	{
		this.holderName = holderName;
		this.cardNumber = cardNumber;
		this.expiryDate = expiryDate;
	}
}

class MembershipCard extends Card{
	
	private int rating;

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
	
	MembershipCard(String holderName,String cardNumber,String expiryDate)
	{
		super(holderName,cardNumber,expiryDate);
		this.rating = rating;
	}
       public String toString() {
		
		return "\n"+ super.holderName+"'s Payback Card Details : "+"\nCard Number :"+super.cardNumber +
				"\nExpiryDate :"+super.expiryDate;
	}
	
}

class PaybackCard extends MembershipCard{
	
	private int pointsEarned;
	private double totalAmount;
	public int getPointsEarned() {
		return pointsEarned;
	}
	public void setPointsEarned(int pointsEarned) {
		this.pointsEarned = pointsEarned;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	PaybackCard(String holderName,String cardNumber,String expiryDate,int pointsEarned,double totalAmount)
	{
		super(holderName,cardNumber,expiryDate);
		this.pointsEarned = pointsEarned;
		this.totalAmount = totalAmount;
	}
	
	@Override
	public String toString() {
		
		return "\n"+ super.holderName+"'s Payback Card Details : "+"\nCard Number :"+super.cardNumber
				 +"\nPoints Earned :"+this.pointsEarned+"\nTotal Amount : "+this.totalAmount; 
	}
}

public class MainBank {

	public static void main(String[] args) {
		
	Scanner sc=new Scanner(System.in);
		
	    System.out.println("Select the Card");
		System.out.println("Press 1 Payback Card");
		System.out.println("Press 2 Membership Card");
		
		System.out.println("--------------------------------------------------------");
		int a=sc.nextInt();
		
		System.out.println("Enter the Card Details: ");
		System.out.println("\nEnter the HolderName ");
		String pstr=sc.next();
		System.out.println("Enter the CardNumber ");
		String pstr1=sc.next();
		System.out.println("Enter the ExpiryDate ");
		String pstr2=sc.next();
		
		switch(a)
		{
		case 1:
			
			System.out.println("\n"+pstr +"|"+pstr1 +"|" +pstr2);
			
			System.out.println("\nEnter points in card ");
			int b=sc.nextInt();
			System.out.println("\nEnter Amount ");
			double c=sc.nextDouble();
			
	        PaybackCard pc=new PaybackCard(pstr, pstr1, pstr2, b, c);
	        System.out.print(pc.toString());	
	        break;
	      
		case 2:
			
			System.out.println("\n"+pstr +"|"+pstr1 +"|" +pstr2);
			MembershipCard mc=new MembershipCard(pstr, pstr1, pstr2);
	        System.out.print(mc.toString());	
	        break;
	      
		}
		sc.close();
		
	}

}

o/p


Select the Card
Press 1 Payback Card
Press 2 Membership Card
--------------------------------------------------------
1
Enter the Card Details: 

Enter the HolderName 
Elango
Enter the CardNumber 
12345
Enter the ExpiryDate 
11/08/2011

Elango|12345|11/08/2011

Enter points in card 
1000

Enter Amount 
40000

Elango's Payback Card Details : 
Card Number :12345
Points Earned :1000
Total Amount : 40000.0



4. The following Java applications contain errors. Point out the statement(s) that contain errors. Explain what each of the errors is, and how it can be fixed.

public class OOPExercises { public static void main(String[] args) { A objA = new A(); System.out.println("in main(): "); System.out.println("objA.a = "+objA.a); objA.a = 222; } }

public class A {

private int a = 100;

public void setA( int value) {

a = value;

}

public int getA() {

return a;

}

} //class A


sol:

package AbstractAndInterfaces;

public class OOPExercises { 
	
	public static void main(String[] args) { 
		
		A objA = new A(); 
		System.out.println("in main(): "); 
	//error//	System.out.println("objA.a = "+objA.a);
		objA.setA(222); 
		System.out.println(objA.getA() ); 
		}
}

class A 
{
private int a = 100;

  public void setA( int value)
   {
     a = value;
}
public int getA() 
{
    return a;
}
}


o/p

in main(): 
222




5. Show the output of the following applications.

public class OOPExercises { public static void main(String[] args) { FirstClass objA = new FirstClass(); SecondClass objB = new SecondClass(); System.out.println("in main(): "); System.out.println("objA.a = "+objA.getFirstClass()); System.out.println("objB.b = "+objB.getSecondClass()); objA.setFirstClass (222); objB.setSecondClass (333.33); System.out.println("objA.a = "+objA.getFirstClass()); System.out.println("objB.b = "+objB.getSecondClass()); } } Output:

public class FirstClass {

int a = 100; public FirstClass () { System.out.println("in the constructor of class FirstClass: "); System.out.println("a = "+a); a = 333; System.out.println("a = "+a); } public void setFirstClass( int value) { a = value; } public int getFirstClass() { return a; } } //class FirstClass

public class SecondClass {

double b = 123.45;

public SecondClass() {

System.out.println("-----in the constructor of class B: ");

System.out.println("b = "+b);

b = 3.14159;

System.out.println("b = "+b);

}

public void setSecondClass( double value) {

b = value;

}

public double getSecondClass() {

return b;

}

} //class SecondClass


sol:

sol:

package AbstractAndInterfaces;

public class OOPExercise { 
	
	public static void main(String[] args) { 
		
		FirstClass objA = new FirstClass();
		SecondClass objB = new SecondClass(); 
		System.out.println("in main(): "); 
		System.out.println("objA.a = "+objA.getFirstClass());
		System.out.println("objB.b = "+objB.getSecondClass()); 
		objA.setFirstClass (222); 
		objB.setSecondClass (333.33);
		System.out.println("objA.a = "+objA.getFirstClass());
		System.out.println("objB.b = "+objB.getSecondClass()); 
		}
} 

class FirstClass {

int a = 100;

public FirstClass () {
	
	System.out.println("in the constructor of class FirstClass: ");
	System.out.println("a = "+a);
	a = 333; 
	System.out.println("a = "+a); 
	
} 

public void setFirstClass( int value) {
	
	a = value;
} 

public int getFirstClass() { 
	
	return a;
	} 
} //class FirstClass

class SecondClass {

double b = 123.45;

public SecondClass()
{

System.out.println("-----in the constructor of class B: ");

System.out.println("b = "+b);

b = 3.14159;

System.out.println("b = "+b);

}

public void setSecondClass( double value) {

b = value;

}

public double getSecondClass() {

return b;

}

}


o/p

in the constructor of class FirstClass: 
a = 100
a = 333
-----in the constructor of class B: 
b = 123.45
b = 3.14159
in main(): 
objA.a = 333
objB.b = 3.14159
objA.a = 222
objB.b = 333.33


6.

public class OOPExercises { static int a = 555; public static void main(String[] args) { A objA = new A(); B objB1 = new B(); A objB2 = new B(); C objC1 = new C(); B objC2 = new C(); A objC3 = new C(); objA.display(); objB1.display(); objB2.display(); objC1.display(); objC2.display(); objC3.display(); } } Output:

public class A {

int a = 100;

public void display() {

System.out.printf("a in A = %d\n", a);

}



sol:

package AbstractAndInterfaces;
public class OOPExercisess {
	
	static int a = 555; 
	
	public static void main(String[] args) {
		A1 objA = new A1(); 
		B objB1 = new B();
		A1 objB2 = new B();
		C objC1 = new C(); 
		B objC2 = new C(); 
		A1 objC3 = new C(); 
		objA.display(); 
		objB1.display(); 
		objB2.display(); 
		objC1.display();
		objC2.display(); 
		objC3.display(); 
		} 
	} 


class A1 {

int a = 100;

public void display() {

System.out.printf("a in A = %d\n", a);

}
}

class B extends A1{

int b = 200;

public void display() {

System.out.printf("a in B = %d\n", b);

}
}

class C extends B {

int c = 300;

public void display() {

System.out.printf("a in C = %d\n", c);

}
}


o/p

a in A = 100
a in B = 200
a in B = 200
a in C = 300
a in C = 300
a in C = 300
