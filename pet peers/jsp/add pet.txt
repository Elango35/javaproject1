<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="java.sql.*" %>
<%@ page import="java.io.*" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
* {
  box-sizing: border-box;
}
body {
  font-family: Arial;
  padding: 6px;
  background: white;
}
.header {
  padding: 10px;
  text-align: center;
  background: white;
}
.header h1 {
  font-size: 50px;
}
.topnav {
  overflow: hidden;
  background-color: #333;
}
td:hover,th:hover,h2:hover {
 color: orange;
}
.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  padding: 14px 16px;
  text-decoration: none;
  background-color: #333;
}
.topnav a:hover {
  background-color: orange;
  color: black;
}
h:hover {
  background-color: orange;
  color: black;
}
.leftcolumn {   
  float: left;
  width: 75%;
}
.rightcolumn {
  float: left;
  width: 25%;
  background-color: #f1f1f1;
  padding-left: 20px;
}
.card {
  background-color: white;
  padding: 20px;
  margin-top: 20px;
}

span
{
  color: orange;
}
.buy:hover{
        background-color: #aaa;
    }
.buy {
        display: block;
        width: 45px;
        background-color: #ADD8E6;
        border: 1px solid black;
        text-align: center;
        text-decoration: none;
        padding: 1px 1px 1px 1px;
   }

.buy:visited {
    color:orange;
}
 div {
        margin-bottom: 10px;
      }
      label {
        display: inline-block;
        width: 110px;
        color: #777777;
      }
      input {
        padding: 5px 10px;
      }
</style>
</head>
<body>
<div class="header">
     <h1>Pet<span>Smart</span></h1>
  <p>"A beautiful<span style='font-size:30px;'>&#128512;</span>smile is the best <span>universal welcome </span>It can make the day a good day easily"</p>
</div>
<h1>PetShop</h1>
<div class="topnav">
    
  <a href="petheader.jsp">HOME</a>
  <a href="AddPetShow.jsp">My Pet</a>
  <a href="AddPet.jsp">Add Pet</a>
  <a href="PetLoginRegister.html" style="float:right">Logout</a>
</div>
 
  <div class="leftcolumn">
    <div class="card">   
   
<form action="AddPetServlet" method="post">
 
   <h1 align="center">Pet Information</h1>
   
    <div align="center">
        <label for="name"><h3>Pet Name:</h3></label>
        <input id="name" name="name" type="text" autofocus required />
      </div>
      <div  align="center">
        <label for="age"><h3>Pet Age:</h3></label>
        <input id="age" name="age" type="text" autofocus required />
      </div>
         <div  align="center">
        <label for="place"><h3>Location:</h3></label>
        <input id="place" name="place" type="text" autofocus required />
      </div>
       <div  align="center">
      <input type="submit" value="Submit" />
      </div>
       </form>  
    </div>
</div>
  <div class="rightcolumn">
    <div class="card">
      <h2>About Me</h2>
      <p>PetSmart, the leading online pet shop in India, offers you top quality pet products, which fulfill each and every parameter that is mentioned above for ensuring the overall wellness of your pet</p>
    </div>
    <div class="card">

    <h2>Popular Quotes</h2>
    <ul>
      <li>"Our perfect companions never have fewer than four feet"</li>
      <li>"Cats are connoisseurs of comfort"</li>
      <li>"Our perfect companions never have fewer than four feet"</li>
    </ul>
    </div>
</div>
</body>
</html>
