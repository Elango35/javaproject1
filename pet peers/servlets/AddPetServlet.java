package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/AddPetServlet")
public class AddPetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		response.setContentType("text/html");  
        PrintWriter out = response.getWriter();    
        int sno=0;
        String name = request.getParameter("name");   
        int age = Integer.parseInt(request.getParameter("age"));
        String place =request.getParameter("place");
        try
        {  
        	String connectionURL = "jdbc:mysql://localhost:3306/petshop";
 
        	Class.forName("com.mysql.jdbc.Driver");

        	Connection con = DriverManager.getConnection(connectionURL, "root", "Vcetvcet@35");

  
             PreparedStatement ps=con.prepareStatement("insert into petupdate(petname,petage,petplace) values(?,?,?)");  

           
             ps.setString(1,name);  
             ps.setInt(2,age);  
             ps.setString(3, place); 
             

             int i = ps.executeUpdate();  
             if(i>0)  
             {
             response.sendRedirect("http://localhost:8085/new/petheader.jsp");
             sno++;
             }
             else
             {
            	 sno=0;
             }
        }
        catch (Exception ex)
        {
             ex.printStackTrace();
        }  
        out.close();  
   }  
	}