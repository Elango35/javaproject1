                                Elangovan v_51922437_Assignments_ Revathi Mam Batch

                                                 DAY 1


1. Write a Java program to print the result of the following operations.

Test Data:

a. -5 + 8 * 6

b. (55+9) % 9

c. 20 + -3*5 / 8

d. 5 + 15 / 3 * 2 - 8 % 3


sol:

package javademo;

public class Operations {

	public static void main(String[] args) {
		
		System.out.println("a ="+ (-5+8*6));
		System.out.println("b ="+ ((55+9)%6));
		System.out.println("c ="+ (20+(-3*5)/8));
		System.out.println("d ="+ ((5+15/3*2)-8%3));

	}

}

o/p

a =43
b =4
c =19
d =13


2. Write a Java program to print the sum (addition), multiply, subtract, divide and remainder of two numbers.

sol:

package javademo;
import java.util.Scanner;


public class Arithmetic {

	public static void main(String[] args) {
		
      Scanner sc = new Scanner(System.in);
      System.out.println("Enter two Numbers :");
      int a=sc.nextInt();
      int b=sc.nextInt();
      System.out.println("Addition ="+ (a+b));
      System.out.println("Subtraction = "+ (a-b));
      System.out.println("Multiplication ="+ (a*b));
      System.out.println("Division ="+ (a/b));
      System.out.println("Modules ="+ (a%b));
	}
}

o/p

Enter two Numbers :
2
5
Addition =7
Subtraction = -3
Multiplication =10
Division =0
Modules =2


3. Write a Java program that takes three numbers as input to calculate and print the average of the numbers.

sol:

package javademo;

public class Average {

	public static void main(String[] args) {
		
		int a[]= {20,30,40};
		int sum=0;
		int average=0;
		for(int i=0;i<a.length;i++)
		{
			sum=sum+a[i];
		}
		System.out.println("Sum of numbers= "+sum);
        System.out.println("Average of numbers= "+ sum/a.length);
	}

}

o/p

Sum of numbers= 90
Average of numbers= 30


4. Write a Java program to swap two variables.

sol:

package javademo;

public class Swapping {

	public static void main(String[] args) {
		
		int a=10;
		int b=4;
		System.out.println(a +" and "+b);
		int temp=a;
		a=b;
		b=temp;
		System.out.println(a +" and "+b);

	}

}
o/p

Before Swapping =10 and 4
After Swapping = 4 and 10


5. Write a java program, which will take a number variable and check whether the number is prime or not.

sol:

package javademo;

public class PrimeNumbers {

			  public static void main(String[] args) {

			    int num = 4;
			    boolean flag = false;
			    for (int i = 2; i <= num / 2; ++i) {
			     
			      if (num % i == 0) {
			        flag = true;
			        break;
			      }
			    }
			    if (!flag)
			      System.out.println(num + " is a prime number.");
			    else
			      System.out.println(num + " is not a prime number.");
			  }
	}

o/p

4 is not a prime number.


6. Write a Java program to print the ascii value of a given character.

sol:

package javademo;

public class AsciiValues {
	
		   public static void main(String args[]){
		      char ch = 'e';
		      int asciiValue = ch;
		      System.out.println("ASCII value of the given character is :"+asciiValue);
		   }
		}
o/p

ASCII value of the given character is :101


7. Write a Java program which iterates the integers from 1 to 100. 
For multiples of three print "Fizz" instead of the number and print "Buzz" for the multiples of five.
 When number is divided by both three and five, print "fizz buzz"

sol:

package javademo;

public class Fizzbuzz {
	    public static void main(String args[]){
	        int i,j;
	        for (i=1;i<=100;i++){
	            if(i%3==0){
	                System.out.println(i+" = fizz");
	                
	            }
	            if(i%5==0){
	                System.out.println(i+" = buzz");
	                
	            }
	            if(i%3==0 && i%5==0)
	            {
	                System.out.println(i+ " = fizz and buzz");
	                
	            }
	            
	        }
	        
	    }
}


o/p

3 = fizz
5 = buzz
6 = fizz
9 = fizz
10 = buzz
12 = fizz
15 = fizz
15 = buzz
15 = fizz and buzz
18 = fizz
20 = buzz
21 = fizz
24 = fizz
25 = buzz
27 = fizz
30 = fizz
30 = buzz
30 = fizz and buzz
33 = fizz
35 = buzz
36 = fizz
39 = fizz
40 = buzz
42 = fizz
45 = fizz
45 = buzz
45 = fizz and buzz
48 = fizz
50 = buzz
51 = fizz
54 = fizz
55 = buzz
57 = fizz
60 = fizz
60 = buzz
60 = fizz and buzz
63 = fizz
65 = buzz
66 = fizz
69 = fizz
70 = buzz
72 = fizz
75 = fizz
75 = buzz
75 = fizz and buzz
78 = fizz
80 = buzz
81 = fizz
84 = fizz
85 = buzz
87 = fizz
90 = fizz
90 = buzz
90 = fizz and buzz
93 = fizz
95 = buzz
96 = fizz
99 = fizz
100 = buzz


8. Write a program to read a number and calculate the sum of odd digits (values) present in the given number.

Create a class with a static method checkSum which accepts a positive integer. The return type should be 1 if the sum is odd. In case the sum is even return -1 as output.

Create a class Main which would get the input as a positive integer and call the static method checkSum present in the UserMainCode.


sol:

package javademo;

class Calculations
{
	public int checkSum(int array[])
	{
		int sum=0;
		for(int i=1;i<array.length;i=i+2)
		{
			if(array[i]!= 0)
			{
				sum=sum+(array[i]);
			}
			else
			{
				return 0;
			}
		}
		if(sum%2==0)
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
	
}
public class SumOfOddDigits {

	public static void main(String[] args) {
		
		Calculations cal=new Calculations();
		int[] array= {5,6,8,9,5};
		int k=cal.checkSum(array);
		if(k==1)
		{
			System.out.println("Sum of numbers is odd ");
		}
		else if(k==-1)
		{
			System.out.println("Sum of numbers is even  ");
		}
		else
		{
			System.out.println("Enter positive numbers ");
		}
	}

}


o/p:

Sum of numbers is odd 


9. Write a program to read a number, calculate the sum of squares of even digits (values) present in the given number.

Create a class UserMainCode with a static method sumOfSquaresOfEvenDigits which accepts a positive integer. The return type (integer) should be the sum of squares of the even digits.

Create a class Main which would get the input as a positive integer and call the static method sumOfSquaresOfEvenDigits present in the UserMainCode.


sol:

package javademo;
	class UserMainCode
	{
		public int sumOfSquaresOfEvenDigits(int array[])
		{
			int sum=0;
			for(int i=0;i<array.length;i++)
			{
				if(array[i]%2 == 0)
				{
					sum=sum+(array[i]*array[i]);
				}		
			}
			return sum;	
		}
	}
	public class SquareDigits {

		public static void main(String[] args) {
			
			UserMainCode op=new UserMainCode();
			int[] array= {5,6,8,9,5};
			System.out.println("sum Of Squares Of EvenDigits present in the UserMainCode"+ op.sumOfSquaresOfEvenDigits(array));

		}

	}



o/p

sum Of Squares Of EvenDigits present in the UserMainCode = 100


10. Write a Program which finds the longest word from a sentence. Your program should read a sentence as input from user and return the longest word. In case there

are two words of maximum length return the word which comes first in the sentence.

Include a class UserMainCode with a static method getLargestWord which accepts a string The return type is the longest word of type string.

Create a Class Main which would be used to accept two Input strings and call the static method present in UserMainCode.


sol:

package javademo;

public class LongestWordInString {
	
	String longestWord(String str)
	{
      int len=0;
      String word="";
      String[] arr=str.split("\\s");
   

      for(String s:arr)
      {
    
    	  if(len<s.length()) 
    	  {
    		  len=s.length();
    		  word=s;
    	  }
      }
      return word;
	}

	public static void main(String[] args) {
	
		LongestWordInString ls= new LongestWordInString();
		String str="ABC DEF";
		System.out.print(ls.longestWord(str));
	}

}

o/p

Longest Word In String = ABC




